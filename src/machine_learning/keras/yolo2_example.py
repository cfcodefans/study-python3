import sys
import os
from keras.models import load_model
from keras.preprocessing import image
from keras.engine.training import Model
import cv2
import numpy as np


class BoundingBox:
    def __init__(self, xmin: int, ymin: int, xmax: int, ymax: int, c=None, classes=None):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax

        self.c = c
        self.classes = classes

        self.label: int = -1
        self.score: int = -1

    def get_label(self) -> int:
        if self.label == -1:
            self.label = np.argmax(self.classes)
        return self.label

    def get_score(self) -> int:
        if self.score == -1:
            self.score = self.classes[self.get_label()]
        return self.score


def _interval_overlap(interval_a: [int], interval_b: [int]) -> int:
    x1, x2 = interval_a
    x3, x4 = interval_b

    if x3 < x1:
        if x4 < x1:
            return 0
        return min(x2, x4) - x1
    else:
        if x2 < x3:
            return 0
        return min(x2, x4) - x3


def bbox_iou(box1: BoundingBox, box2: BoundingBox) -> float:
    intersect_w = _interval_overlap([box1.xmin, box1.xmax], [box2.xmin, box2.xmax])
    intersect_h = _interval_overlap([box1.ymin, box1.ymax], [box2.ymin, box2.ymax])

    intersect = intersect_w * intersect_h

    w1, h1 = box1.xmax - box1.xmin, box1.ymax - box1.ymin
    w2, h2 = box2.xmax - box2.xmin, box2.ymax - box2.ymin

    union = w1 * h1 + w2 * h2 - intersect

    return float(intersect) / union


def _softmax(x, axis=-1, t=-100.):
    x = x - np.max(x)

    if np.min(x) < t:
        x = x / np.min(x) * t

    e_x = np.exp(x)

    return e_x / e_x.sum(axis, keepdims=True)


def _sigmoid(x: float) -> float:
    return 1. / (1. + np.exp(-x))


def interpret_output_yolov2(
        output: np.ndarray,
        img_width: int,
        img_height: int
) -> np.ndarray:
    anchors: [float] = [0.57273, 0.677385, 1.87446, 2.06253, 3.33843, 5.47434, 7.88282, 3.52778, 9.77052, 9.16828]
    netout: np.ndarray = output
    nb_class: int = 1
    obj_threshold: float = 0.4
    nms_threshold: float = 0.3
    grid_h, grid_w, nb_box = netout.shape[:3]

    size: int = 4 + nb_class + 1
    nb_box: int = 5

    print(f"""
        grid_w: {grid_w},
        grid_h: {grid_h},
        nb_box: {nb_box},
        size:   {size}
    """)

    netout = netout.reshape(grid_h, grid_w, nb_box, size)

    boxes: [BoundingBox] = []
    # decode the output by the network
    netout[..., 4] = _sigmoid(netout[..., 4])
    netout[..., 5:] = netout[..., 4][..., np.newaxis] * _softmax(netout[..., 5:])
    netout[..., 5:] *= netout[..., 5:] > obj_threshold

    for row in range(grid_h):
        for col in range(grid_w):
            for b in range(nb_box):
                # from 4th element onwards are confidence and class classes
                classes: np.ndarray = netout[row, col, b, 5]
                if np.sum(classes) > 0:
                    # first 4 elements are x, y, w and h
                    x, y, w, h = netout[row, col, b, :4]

                    x = (col + _sigmoid(x)) / grid_w  # center position, unit: image width
                    y = (row + _sigmoid(y)) / grid_h  # center position, unit: image height
                    w = anchors[2 * b + 0] * np.exp(w) / grid_w  # unit: image width
                    h = anchors[2 * b + 1] * np.exp(h) / grid_h  # unit: image height
                    confidence = netout[row, col, b, 4]

                    box = BoundingBox(x - w / 2, y - h / 2, x + w / 2, y + h / 2, confidence, classes)
                    boxes.append(box)

    for c in range(nb_class):
        sorted_indices = list(reversed(np.argsort([box.classes[c] for box in boxes])))
        len_sorted_indices = len(sorted_indices)
        for i in range(len_sorted_indices):
            index_i = sorted_indices[i]
            if boxes[index_i].classes[c] == 0:
                continue
            for j in range(i + 1, len_sorted_indices):
                index_j = sorted_indices[j]
            if bbox_iou(boxes[index_i], boxes[index_j]) >= nms_threshold:
                boxes[index_j].classes[c] = 0


def main(argv: [str]):
    print(os.listdir("./models"))
    model_face: Model = load_model(filepath="./models/yolov2_tiny-face.h5")
    img: np.ndarray = cv2.imread("./imgs/board1.jpg")

    img = img[..., ::-1]
    # img = cv2.cvtColor(src=img,
    #              code=cv2.COLOR_RGB2RGBA,
    #              dst=None,
    #              dstCn=4)
    img = cv2.resize(src=img, dsize=(416, 416))
    img = np.expand_dims(img, axis=0)
    out: np.ndarray = model_face.predict(img)
    print(out.shape)


if __name__ == '__main__':
    main(sys.argv[1:])
