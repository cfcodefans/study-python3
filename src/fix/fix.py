import sys

if __name__ == '__main__':
    node = sys.argv[1]
    line = sys.stdin.readline()
    while line != "":
        if "blk" not in line:
            continue
        size_path = line.split(' ')
        print(' '.join([node, size_path[1].trip(), size_path[0], 'unchecked']))
        line = sys.stdin.readline()
